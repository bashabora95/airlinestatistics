package com.airlinestatistics;

import android.app.Application;
import android.graphics.Typeface;

import com.airlinestatistics.data.AirlineStatisticModel;
import com.google.gson.Gson;

import java.util.List;

public class AppController extends Application {

    private static AppController instance;
    private Gson globalGsonInstance;
    private List<AirlineStatisticModel> statisticsData;

    public Typeface mTfRegular;
    public Typeface mTfLight;

    public static AppController getInstance() {
        return instance;
    }

    public Gson getGsonInstance() {
        return globalGsonInstance;
    }

    public List<AirlineStatisticModel> getStatisticsData() {
        return statisticsData;
    }

    public void setStatisticsData(List<AirlineStatisticModel> statisticsData) {
        this.statisticsData = statisticsData;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        globalGsonInstance = new Gson();
        mTfRegular = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        mTfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");
    }
}
