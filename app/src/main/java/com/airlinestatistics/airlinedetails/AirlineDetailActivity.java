package com.airlinestatistics.airlinedetails;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.airlinestatistics.AppController;
import com.airlinestatistics.R;
import com.airlinestatistics.data.AirlineStatisticModel;
import com.airlinestatistics.databinding.ActivityAirlineDetailBinding;
import com.airlinestatistics.utils.RadarMarkerView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;

import java.util.ArrayList;
import java.util.List;

public class AirlineDetailActivity extends AppCompatActivity {

    public static final String INDEX = "index";
    private ActivityAirlineDetailBinding binding;
    private List<AirlineStatisticModel> dataset;
    private AirlineStatisticModel datapoint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_airline_detail);

        int index = getIntent().getIntExtra(INDEX, 0);
        dataset = AppController.getInstance().getStatisticsData();
        datapoint = dataset.get(index);

        binding.radarChart.getDescription().setEnabled(false);

        binding.radarChart.setWebLineWidth(1f);
        binding.radarChart.setWebColor(Color.LTGRAY);
        binding.radarChart.setWebLineWidthInner(1f);
        binding.radarChart.setWebColorInner(Color.LTGRAY);
        binding.radarChart.setWebAlpha(100);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MarkerView mv = new RadarMarkerView(this, R.layout.radar_markerview);
        mv.setChartView(binding.radarChart); // For bounds control
        binding.radarChart.setMarker(mv); // Set the marker to the chart

        setData();

        binding.radarChart.animateXY(1400, 1400, Easing.EasingOption.EaseInOutQuad, Easing.EasingOption.EaseInOutQuad);

        XAxis xAxis = binding.radarChart.getXAxis();
        xAxis.setTypeface(AppController.getInstance().mTfLight);
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            private String[] mActivities = new String[]{"Flights", "Routes", "Airports To", "Airports From", "States To", "States From"};

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mActivities[(int) value % mActivities.length];
            }
        });
        xAxis.setTextColor(Color.WHITE);

        YAxis yAxis = binding.radarChart.getYAxis();
        yAxis.setTypeface(AppController.getInstance().mTfLight);
        yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(80f);
        yAxis.setDrawLabels(false);

        Legend l = binding.radarChart.getLegend();
        l.setEnabled(false);
    }

    public void setData() {

            /*

    "numberOfFlights": 6279,
    "numberOfRoutes": 222,
    "numberOfAirportsFlownTo": 17,
    "numberOfAirportsFlownFrom": 17,
    "numberOfStatesFlownTo": 8,
    "numberOfStatesFlownFrom": 8
             */
        int maxFlights = 0;
        int maxRoutes = 0;
        int maxAirportsTo = 0;
        int maxAirportsFrom = 0;
        int maxStatesTo = 0;
        int maxStatesFrom = 0;

        String textToShow = "Airline Code: " + datapoint.getAirlineCode() + "\n" +
                "Number Of Flights: " + datapoint.getNumberOfFlights() + "\n" +
                "Number Of Routes: " + datapoint.getNumberOfRoutes() + "\n" +
                "Number Of Airports Flown To: " + datapoint.getNumberOfAirportsFlownTo() + "\n" +
                "Number Of Airports Flown From: " + datapoint.getNumberOfAirportsFlownFrom() + "\n" +
                "Number Of States Flown From: " + datapoint.getNumberOfStatesFlownFrom() + "\n" +
                "Number Of States Flown To: " + datapoint.getNumberOfStatesFlownTo() + "\n";
        binding.tvAirlineId.setText(textToShow);

        for (AirlineStatisticModel item : dataset) {
            if (item.getNumberOfFlights() > maxFlights) maxFlights = item.getNumberOfFlights();
            if (item.getNumberOfRoutes() > maxRoutes) maxRoutes = item.getNumberOfRoutes();
            if (item.getNumberOfAirportsFlownTo() > maxAirportsTo) maxAirportsTo = item.getNumberOfAirportsFlownTo();
            if (item.getNumberOfAirportsFlownFrom() > maxAirportsFrom) maxAirportsFrom = item.getNumberOfAirportsFlownFrom();
            if (item.getNumberOfStatesFlownTo() > maxStatesTo) maxStatesTo = item.getNumberOfStatesFlownTo();
            if (item.getNumberOfStatesFlownFrom() > maxStatesFrom) maxStatesFrom = item.getNumberOfStatesFlownFrom();
        }

        ArrayList<RadarEntry> entries1 = new ArrayList<RadarEntry>();

        entries1.add(new RadarEntry((int)(100 * datapoint.getNumberOfFlights() / (float)maxFlights)));
        entries1.add(new RadarEntry((int)(100 * datapoint.getNumberOfRoutes() / (float)maxRoutes)));
        entries1.add(new RadarEntry((int)(100 * datapoint.getNumberOfAirportsFlownTo() / (float)maxAirportsTo)));
        entries1.add(new RadarEntry((int)(100 * datapoint.getNumberOfAirportsFlownFrom() / (float)maxAirportsFrom)));
        entries1.add(new RadarEntry((int)(100 * datapoint.getNumberOfStatesFlownTo() / (float)maxStatesTo)));
        entries1.add(new RadarEntry((int)(100 * datapoint.getNumberOfStatesFlownFrom() / (float)maxStatesFrom)));

        RadarDataSet set1 = new RadarDataSet(entries1, "Airline Id: " + datapoint.getAirlineCode());
        set1.setColor(Color.rgb(103, 110, 129));
        set1.setFillColor(Color.rgb(103, 110, 129));
        set1.setDrawFilled(true);
        set1.setFillAlpha(180);
        set1.setLineWidth(2f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<IRadarDataSet>();
        sets.add(set1);

        RadarData data = new RadarData(sets);
        data.setValueTypeface(AppController.getInstance().mTfLight);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE);

        binding.radarChart.setData(data);
        binding.radarChart.invalidate();
    }
}
