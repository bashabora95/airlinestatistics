package com.airlinestatistics.airlinedetails;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.airlinestatistics.R;
import com.airlinestatistics.data.AirlineStatisticModel;
import com.airlinestatistics.databinding.ViewholderAirlineStatisticsBinding;

import java.util.ArrayList;
import java.util.List;

public class AirlineStatisticsRVAdapter extends RecyclerView.Adapter<AirlineStatisticsRVViewHolder> {
    private List<AirlineStatisticModel> items = new ArrayList<>();

    public AirlineStatisticsRVAdapter() {
    }

    @NonNull
    @Override
    public AirlineStatisticsRVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewholderAirlineStatisticsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.viewholder_airline_statistics, parent, false);
        return new AirlineStatisticsRVViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AirlineStatisticsRVViewHolder holder, int position) {
        holder.bind(items.get(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<AirlineStatisticModel> items) {
        this.items = items;
        notifyItemRangeInserted(0, items.size());
    }
}
