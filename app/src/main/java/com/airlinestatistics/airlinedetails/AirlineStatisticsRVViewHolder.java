package com.airlinestatistics.airlinedetails;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.airlinestatistics.airlinedetails.AirlineDetailActivity;
import com.airlinestatistics.data.AirlineStatisticModel;
import com.airlinestatistics.databinding.ViewholderAirlineStatisticsBinding;

public class AirlineStatisticsRVViewHolder extends RecyclerView.ViewHolder {
    private final ViewholderAirlineStatisticsBinding binding;
    private int index = 0;

    public AirlineStatisticsRVViewHolder(ViewholderAirlineStatisticsBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
        binding.getRoot().setOnClickListener(v -> {
            Intent intent = new Intent(itemView.getContext(), AirlineDetailActivity.class);
            intent.putExtra(AirlineDetailActivity.INDEX, index);
            v.getContext().startActivity(intent);
        });
    }

    public void bind(AirlineStatisticModel data, int position) {
        binding.tvAirlineCode.setText("Airline : " + data.getAirlineCode());
        index = position;
    }
}
