package com.airlinestatistics.airlinedetails;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.airlinestatistics.AppController;
import com.airlinestatistics.R;
import com.airlinestatistics.databinding.ActivityAirlinesListBinding;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class AirlinesListActivity extends AppCompatActivity {

    private ActivityAirlinesListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_airlines_list);

        binding.rvStatistics.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        AirlineStatisticsRVAdapter adapter = new AirlineStatisticsRVAdapter();
        binding.rvStatistics.setAdapter(adapter);
        binding.rvStatistics.setItemAnimator(new SlideInUpAnimator());
        binding.rvStatistics.getItemAnimator().setAddDuration(300);
        binding.rvStatistics.post(() -> adapter.setItems(AppController.getInstance().getStatisticsData()));
    }
}
