package com.airlinestatistics.barchart;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.airlinestatistics.R;
import com.airlinestatistics.databinding.ActivityAirportsAndStatesFlownBinding;

public class AirportsAndStatesFlownActivity extends AppCompatActivity {

    private ActivityAirportsAndStatesFlownBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_airports_and_states_flown);
    }
}
