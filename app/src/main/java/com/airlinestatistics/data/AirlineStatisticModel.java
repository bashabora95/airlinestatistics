package com.airlinestatistics.data;

public class AirlineStatisticModel {
    private String airlineCode;
    private int numberOfFlights;
    private int numberOfRoutes;
    private int numberOfAirportsFlownTo;
    private int numberOfAirportsFlownFrom;
    private int numberOfStatesFlownTo;
    private int numberOfStatesFlownFrom;

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public int getNumberOfFlights() {
        return numberOfFlights;
    }

    public void setNumberOfFlights(int numberOfFlights) {
        this.numberOfFlights = numberOfFlights;
    }

    public int getNumberOfRoutes() {
        return numberOfRoutes;
    }

    public void setNumberOfRoutes(int numberOfRoutes) {
        this.numberOfRoutes = numberOfRoutes;
    }

    public int getNumberOfAirportsFlownTo() {
        return numberOfAirportsFlownTo;
    }

    public void setNumberOfAirportsFlownTo(int numberOfAirportsFlownTo) {
        this.numberOfAirportsFlownTo = numberOfAirportsFlownTo;
    }

    public int getNumberOfAirportsFlownFrom() {
        return numberOfAirportsFlownFrom;
    }

    public void setNumberOfAirportsFlownFrom(int numberOfAirportsFlownFrom) {
        this.numberOfAirportsFlownFrom = numberOfAirportsFlownFrom;
    }

    public int getNumberOfStatesFlownTo() {
        return numberOfStatesFlownTo;
    }

    public void setNumberOfStatesFlownTo(int numberOfStatesFlownTo) {
        this.numberOfStatesFlownTo = numberOfStatesFlownTo;
    }

    public int getNumberOfStatesFlownFrom() {
        return numberOfStatesFlownFrom;
    }

    public void setNumberOfStatesFlownFrom(int numberOfStatesFlownFrom) {
        this.numberOfStatesFlownFrom = numberOfStatesFlownFrom;
    }

}