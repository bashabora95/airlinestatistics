package com.airlinestatistics.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.airlinestatistics.AppController;
import com.airlinestatistics.R;
import com.airlinestatistics.airlinedetails.AirlinesListActivity;
import com.airlinestatistics.barchart.AirportsAndStatesFlownActivity;
import com.airlinestatistics.data.AirlineStatisticModel;
import com.airlinestatistics.databinding.ActivityMainBinding;
import com.airlinestatistics.pie.NumFlightsActivity;
import com.airlinestatistics.utils.FileUtils;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        AppController.getInstance().setStatisticsData(FileUtils.loadAirlineStatisticsFromFile());

        binding.cvPieChart.setOnClickListener(v -> startActivity(new Intent(this, NumFlightsActivity.class)));
        binding.cvRadarChart.setOnClickListener(v -> startActivity(new Intent(this, AirlinesListActivity.class)));
        binding.cvBarChart.setOnClickListener(v -> startActivity(new Intent(this, AirportsAndStatesFlownActivity.class)));
    }
}
