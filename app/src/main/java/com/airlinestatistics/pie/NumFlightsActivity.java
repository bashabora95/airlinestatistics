package com.airlinestatistics.pie;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.airlinestatistics.AppController;
import com.airlinestatistics.R;
import com.airlinestatistics.data.AirlineStatisticModel;
import com.airlinestatistics.databinding.ActivityNumFlightsBinding;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;
import java.util.List;

public class NumFlightsActivity extends AppCompatActivity {

    private ActivityNumFlightsBinding binding;
    private int totalFlights = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_num_flights);

        binding.pieChartNumFlights.setUsePercentValues(true);
        binding.pieChartNumFlights.getDescription().setEnabled(false);
        binding.pieChartNumFlights.setExtraOffsets(5, 10, 5, 5);

        binding.pieChartNumFlights.setDragDecelerationFrictionCoef(0.95f);

        binding.pieChartNumFlights.setCenterTextTypeface(AppController.getInstance().mTfLight);
        binding.pieChartNumFlights.setCenterText("Flights per Airline");

        binding.pieChartNumFlights.setDrawHoleEnabled(true);
        binding.pieChartNumFlights.setHoleColor(Color.WHITE);

        binding.pieChartNumFlights.setTransparentCircleColor(Color.WHITE);
        binding.pieChartNumFlights.setTransparentCircleAlpha(110);

        binding.pieChartNumFlights.setHoleRadius(58f);
        binding.pieChartNumFlights.setTransparentCircleRadius(61f);

        binding.pieChartNumFlights.setDrawCenterText(true);

        binding.pieChartNumFlights.setRotationAngle(0);
        // enable rotation of the chart by touch
        binding.pieChartNumFlights.setRotationEnabled(true);
        binding.pieChartNumFlights.setHighlightPerTapEnabled(true);

        // binding.pieChartNumFlights.setUnit(" €");
        // binding.pieChartNumFlights.setDrawUnitsInChart(true);


        setData();
        setOnChartValueSelected();

        binding.pieChartNumFlights.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // binding.pieChartNumFlights.spin(2000, 0, 360);

        Legend l = binding.pieChartNumFlights.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        binding.pieChartNumFlights.setEntryLabelColor(Color.WHITE);
        binding.pieChartNumFlights.setEntryLabelTypeface(AppController.getInstance().mTfRegular);
        binding.pieChartNumFlights.setEntryLabelTextSize(12f);
    }

    private void setOnChartValueSelected() {
        binding.pieChartNumFlights.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                PieEntry pieEntry = ((PieEntry)e);

                binding.tvSelectedItem.setText(String.format("Airline Code: \"%s\"\nhas completed %d(%.1f) flights"
                        , pieEntry.getLabel()
                        , ((int)pieEntry.getValue())
                        , 100 * pieEntry.getValue() / totalFlights));
            }

            @Override
            public void onNothingSelected() {
                binding.tvSelectedItem.setText("");
            }
        });
    }

    private void setData() {
        List<AirlineStatisticModel> dataset = AppController.getInstance().getStatisticsData();
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < dataset.size() ; i++) {
            AirlineStatisticModel datapoint = dataset.get(i);
            totalFlights += datapoint.getNumberOfFlights();
            entries.add(new PieEntry(datapoint.getNumberOfFlights(),
                    datapoint.getAirlineCode(),
                    getResources().getDrawable(R.drawable.pie_chart)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);

        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(AppController.getInstance().mTfLight);
        binding.pieChartNumFlights.setData(data);

        // undo all highlights
        binding.pieChartNumFlights.highlightValues(null);

        binding.pieChartNumFlights.invalidate();
    }
}
