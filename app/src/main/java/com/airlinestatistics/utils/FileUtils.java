package com.airlinestatistics.utils;

import com.airlinestatistics.AppController;
import com.airlinestatistics.data.AirlineStatisticModel;
import com.google.gson.reflect.TypeToken;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

public class FileUtils {


    public static List<AirlineStatisticModel> loadAirlineStatisticsFromFile() {
        InputStream is = null;
        String json = null;
        try {
            is = AppController.getInstance().getAssets().open("airline_statistics.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            // should never happen!!
            Logger.error("Error with opening assets", e);
        } finally {
            FileUtils.closeStream(is);
        }

        Type type = new TypeToken<List<AirlineStatisticModel>>() {}.getType();
        return JSONParser.fromJson(json, type);
    }


    /**
     * Close stream
     *
     * @param closeable closeable
     */
    public static void closeStream(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Logger.error("Stream can't be closed", e);
            }
        }
    }
}
