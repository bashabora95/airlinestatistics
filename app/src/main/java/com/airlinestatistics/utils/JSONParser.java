package com.airlinestatistics.utils;

import com.airlinestatistics.AppController;

import java.lang.reflect.Type;

/**
 * Created by matejstern on 05/02/2018.
 */

public class JSONParser {

    public static String toJson(Object o) {
        return AppController.getInstance().getGsonInstance().toJson(o);
    }

    public static String toJson(Object o, Type type) {
        return AppController.getInstance().getGsonInstance().toJson(o, type);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return AppController.getInstance().getGsonInstance().fromJson(json, clazz);
    }

    public static <T> T fromJson(String json, Type type) {
        return AppController.getInstance().getGsonInstance().fromJson(json, type);
    }
}
