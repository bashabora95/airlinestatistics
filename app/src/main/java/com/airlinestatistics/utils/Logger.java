package com.airlinestatistics.utils;

import android.util.Log;

import com.airlinestatistics.BuildConfig;

/**
 * Created by matejstern on 05/02/2018.
 */

public class Logger {

    private static final String TAG = "WHITE";

    public static void logD(String message) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, message);
        }
    }

    public static void error(String message, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, message, t);
        }
    }
}
